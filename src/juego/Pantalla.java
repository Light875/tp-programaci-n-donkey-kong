package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Pantalla {

	private Entorno entorno;
	private Plataforma plataforma[];
	private Mario mario;
	private Escalera escalera[];
	private Barril barril[];
	private Mono mono;
	private Image img;
	private Image img2;

	Pantalla(Entorno entorno) {

		this.entorno = entorno;
		this.plataforma = new Plataforma[6];
		this.escalera = new Escalera[plataforma.length - 1];
		this.barril = new Barril[6];
		generarPlataformas(entorno);
		generarEscaleras();
		this.mono = new Mono(plataforma[plataforma.length - 1].getX() / 2,
				plataforma[plataforma.length - 1].getY() - plataforma[plataforma.length - 1].getAlto() - 25);
		this.mario = new Mario(plataforma[0].getX() / 4, plataforma[0].getY() - 20, entorno);
		this.img = Herramientas.cargarImagen("mario.png");
		this.img2 = Herramientas.cargarImagen("marioquieto.png");
	}


	private void generarPlataformas(Entorno entorno) {
		int posicionY = entorno.alto();
		for (int i = 0; i < plataforma.length; i++) {
			if (i == 0) {
				plataforma[0] = new Plataforma(entorno, (entorno.ancho() / 2), entorno.alto());
			} else if (i % 2 == 0) {
				plataforma[i] = new Plataforma(entorno, entorno.ancho() * 6 / 10, posicionY); //
			} else {
				plataforma[i] = new Plataforma(entorno, entorno.ancho() * 4 / 10, posicionY);
			}
			posicionY = posicionY - entorno.alto() / 6;
		}
	}

	private void generarEscaleras() {
		for (int j = 0; j < plataforma.length - 1; j++) {
			if (j % 2 != 0)
				escalera[j] = new Escalera(plataforma[j].getX() - plataforma[j].getX() / 2,
						plataforma[j].getY() - plataforma[j].getAlto() / 2 - entorno.alto() / 12, entorno.alto() / 6);
			else
				escalera[j] = new Escalera(plataforma[j].getAncho() * 7 / 8,
						plataforma[j].getY() - plataforma[j].getAlto() / 2 - entorno.alto() / 12, entorno.alto() / 6);
		}
	}
	
	public void generarBarriles(Entorno entorno) {

		for (int i = 0; i < barril.length; i++) {
			if (barril[i] == null) {
				if (mono != null && mario != null)
					barril[i] = mono.lanzar();

				break;
			}

		}
	}

	private void dibujarEntorno() {
		for (int i = 0; i < plataforma.length; i++) {
			plataforma[i].dibujarse(entorno);
		}
		for (int i = 0; i < escalera.length; i++) {
			escalera[i].dibujarse(entorno);
		}
	}

	private void dibujarPersonajes() {
		
		if (mono != null) {
			mono.dibujarse(entorno);
		}
	}

	private void dibujarBarriles(Entorno entorno) {
		for (int i = 0; i < barril.length; i++) {
			if (barril[i] != null) {
				barril[i].dibujarse(entorno);
			}
		}
	}

	public void dibujarPantalla(Entorno entorno) {
		dibujarEntorno();
		dibujarPersonajes();
		dibujarBarriles(entorno);
	}


	public void moverMario() {
		if (mario != null) {
			
			if (mario.sobrePlataforma(plataforma)) {
				if(!entorno.estaPresionada(entorno.TECLA_DERECHA)&& !entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
					mario.dibujarse(img);
				}
				if (entorno.estaPresionada(entorno.TECLA_DERECHA) && mario.getX() <= entorno.ancho() - 3)
					mario.moverH(+2,'d');
				
				if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) && mario.getX() >= entorno.ancho() - entorno.ancho() + 5)
					mario.moverH(-2,'i');

			}
			if (mario.sobreEscalera(escalera) && entorno.estaPresionada(entorno.TECLA_ARRIBA)) {
				mario.moverV(-2);
				if (mario.sobreEscalera(escalera) && entorno.estaPresionada(entorno.TECLA_ABAJO)) {
					mario.moverV(+2);
				}
			}
				
			if (mario.sobreEscalera(escalera) && entorno.estaPresionada(entorno.TECLA_ABAJO)) {
				mario.moverV(+2);
				if (mario.sobreEscalera(escalera) && entorno.estaPresionada(entorno.TECLA_ARRIBA)) {
					mario.moverV(-2);
				}
			}
			if (mario.sobreEscalera(escalera) && entorno.estaPresionada(entorno.TECLA_DERECHA)) {
			
				mario.moverH(+1,'d');
			}
			if (mario.sobreEscalera(escalera) && entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
		
				mario.moverH(-1,'i');
			}
				
			if (entorno.sePresiono(entorno.TECLA_ESPACIO) && mario.sobreEscalera(escalera) == false) {
				if (entorno.estaPresionada(entorno.TECLA_DERECHA) && mario.sobrePlataforma(plataforma))
					mario.saltarDiagonalDer();
				
				if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) && mario.sobrePlataforma(plataforma))
					mario.saltarDiagonalIzq();
				
				if (mario.sobrePlataforma(plataforma))
					mario.saltar();
			}
			if (!mario.sobrePlataforma(plataforma) && !mario.sobreEscalera(escalera))
				mario.caer();
			if (entorno.estaPresionada(entorno.TECLA_ABAJO) && mario.getY() < entorno.alto() && mario.sobreEscalera(escalera))
				mario.moverV(+1);
			if (mario.sobreEscalera(escalera) && !entorno.estaPresionada(entorno.TECLA_ABAJO) && !entorno.estaPresionada(entorno.TECLA_ARRIBA)
					&& !entorno.estaPresionada(entorno.TECLA_IZQUIERDA) && !entorno.estaPresionada(entorno.TECLA_DERECHA)) {
				mario.dibujarse(img2);
			}
		}
	}

	public void sobrePlataforma(Entorno entorno) {
		for (int i = 0; i < barril.length; i++) {
			if (barril[i] != null) {
				if (barril[i].sobrePlataforma(plataforma)) {
					barril[i].moverse(plataforma);
					if (barril[i].getX() < entorno.ancho() * 1 / 6 && barril[i].getY() >= entorno.alto() - 30) 
						barril[i] = null;
				} else {
					barril[i].caer();
				}
			}
		}
	}
	
	public void curarAlMono() {
		if (mono != null && mario != null) {
			if (mono.monoCurado(mario))
				mono = null;
		}
	}

	public void marioMuerte() {
		for (int i = 0; i < barril.length; i++) {
			if (mario != null && barril[i] != null && mono != null && mario.lePega(barril[i]))
				mario = null;
		}
	}

	public boolean enJuego() {
		if (mario == null || mono == null) {
			return false;
		}
		return true;
	
	}

	public boolean FinJuego() {
		if (enJuego()== false) {
			return true;
		}
		else {
			return false;
		}
	}	
}
