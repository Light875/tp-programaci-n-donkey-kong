package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Escalera {

	private int x;
	private int y;
	private int alto;
	private int ancho;
	private Image img;
	
	public Escalera(int x, int y,int alto) 
	{
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = 20;
		this.img = Herramientas.cargarImagen("escalera.png");
	}
	
	public void dibujarse(Entorno entorno) 
	{
		entorno.dibujarImagen(img, x, y, 0, 2);
	}
	
	public int getX() 
	{
		return this.x;
	}
	
	public int getY() 
	{
		return this.y;
	}
	
	public int getAncho() 
	{
		return this.ancho;
	}
	
	public int getAlto() 
	{
		return this.alto;
	}

}
