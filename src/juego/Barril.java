package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Barril {
	// Variables de instancia
	private int x;
	private int y;
	private double tam;
	private boolean avanzando;
	private Image img;
	
	public Barril(int x, int y) 
	{
		this.x = x;
		this.y = y;
		this.tam = 20;
		this.avanzando = true;
		this.img = Herramientas.cargarImagen("barril.gif");
	}
	
	public void dibujarse(Entorno entorno) 
	{
		entorno.dibujarCirculo(this.x, this.y, 20, Color.white);
		entorno.dibujarImagen(img, x, y, 0, 2);
	}
	
	public void barrilSuelto() {
		
	}
	public boolean sobrePlataforma(Plataforma[] plataforma) //se llama igual que el anterior pero hacen cosas totalmente distintas, este es un booleano que te dice si esta o no el barril sobre la plataforma
	{
		for (int i=0; i<plataforma.length;i++)
		{
			if (this.x>=plataforma[i].getX()-plataforma[i].getAncho()/2  & this.x<=plataforma[i].getX()+plataforma[i].getAncho()/2 & this.y<=plataforma[i].getY()-plataforma[i].getAlto()/2 & this.y>= plataforma[i].getY()-plataforma[i].getAlto() )
			{	
				return true;	
			}	
		}
		return false;
	}
	
	
	public void moverse(Plataforma [] plataforma) 
	{
		if (avanzando)
			this.x+=2;
		else
			this.x-=2;
		
		for(int i=0;i<plataforma.length;i++) //lo que se hace aca es jugar con el ancho de la plataforma, si el barril llega ahi cambia el booleano
		{
			if(this.x>plataforma[i].getX()+plataforma[i].getAncho()/2)
				avanzando=false;
			if(this.x<plataforma[i].getX()-plataforma[i].getAncho()/2)
				avanzando=true;
		}
	}

	public void caer() 
	{
		this.y+=2;
	}
	
	public int getX() 
	{
		return this.x;
	}
	
	public int getY() 
	{
		return this.y;
	}
	public double getTam() 
	{
		return this.tam;
	}
}
