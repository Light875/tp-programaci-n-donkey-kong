package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Mario {
	private Entorno entorno;
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private Image img2;
	private Image img3;
	private Image img4;
	private Image img5;
	private Image img6;

	public Mario(int x, int y, Entorno entorno) {
		this.x = x;
		this.y = y;
		this.alto = 20;
		this.ancho = 20;
		this.entorno=entorno;
		this.img2 = Herramientas.cargarImagen("mariosaltando.png");
		this.img3 = Herramientas.cargarImagen("mariosaltando2.png");
		this.img4 = Herramientas.cargarImagen("mariocorriendo.gif");
		this.img5 = Herramientas.cargarImagen("mariocorriendo2.gif");
		this.img6 = Herramientas.cargarImagen("mariosubiendo.gif");
	}

	public void dibujarse(Image img) {
		entorno.dibujarImagen(img, x, y-5, 0, 1.8);
	}
	

	public void moverH(int direccion, char d) {
		if(d=='i') {
			this.x = this.x + direccion;
			dibujarse(img5);
		}
		if(d=='d') {
			this.x = this.x + direccion;
			dibujarse(img4);	
		}
		
		
	}

	public void moverV(int direccion) {
		this.y = this.y + direccion;
		dibujarse(img6);
	}

	public boolean sobrePlataforma(Plataforma[] plataforma) // booleano q dice si mario esta o no en las plataformas
	{
		for (int i = 0; i < plataforma.length; i++) {
			if (this.x >= plataforma[i].getX() - plataforma[i].getAncho() / 2
					&& this.x <= plataforma[i].getX() + plataforma[i].getAncho() / 2
					&& this.y <= plataforma[i].getY() - plataforma[i].getAlto() / 2
					&& this.y >= plataforma[i].getY() - plataforma[i].getAlto())
				return true;
		}
		return false;
	}
			
	public boolean sobreEscalera(Escalera[] escalera) // otro booleano que indica si mario esta o no sobre la escalera
	{
		for (int i = 0; i < escalera.length; i++) {
			if (this.x >= escalera[i].getX() - (escalera[i].getAncho()+10) / 2
					&& this.x <= escalera[i].getX() + (escalera[i].getAncho()+10) / 2
					&& this.y + this.alto / 2 <= escalera[i].getY() + (escalera[i].getAlto()+10)/ 2
					&& this.y + this.alto / 2 >= escalera[i].getY() - (escalera[i].getAlto()+10)/ 2)
				return true;
		}
		return false;
	}

	public boolean lePega(Barril barril) {
		int xx = this.x - barril.getX();
		int yy = this.y - barril.getY();
		double dist = Math.sqrt(xx * xx + yy * yy);

		return dist <= this.ancho / 2;
	}
	
	public void saltar() {
		for (int n = 0;n<35;n++) {
			this.y -= 3.5;
			n++;
			dibujarse(img2);
		}
	}
	
	public void saltarDiagonalDer()
	{
		for (int n = 0;n<35;n++) {
			this.y -= 3.5;
			this.x += 2.5;
			n++;
			dibujarse(img2);
		}
	}

	public void saltarDiagonalIzq()
	{
		for (int n = 0;n<35;n++) {
			this.y -= 3.5;
			this.x -= 2.5;
			n++;
			dibujarse(img3);
			
		}
	}
	public void caer() {
		this.y = this.y + 3;
		dibujarse(img2);
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public int getAncho() {
		return this.ancho;
	}


	public int getAlto() {
		return this.alto;
	}
	
}
