package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Mono {
	
	private int x;
	private int y;
	private Image img;

	public Mono(int x, int y) {
		this.x = x;
		this.y = y;
		this.img = Herramientas.cargarImagen("mono.gif");
	}

	public void dibujarse(Entorno entorno) {
		entorno.dibujarImagen(img, x, y, 0, 0.5);
	}

	public Barril lanzar() // devuelve UN barril
	{
		int x_barril = this.x;
		int y_barril = this.y;

		return new Barril(x_barril, y_barril);
	}

	public boolean monoCurado(Mario mario) {/** Sirve para saber si mario hace contacto con donkey **/
		if ((this.x + this.x / 2 >= mario.getX()) && (this.x <= (mario.getX() + mario.getAncho()))) {
			if ((this.y + this.y / 2 >= mario.getY()) && (this.y <= mario.getY() + mario.getAlto())) {
				return true;
			}
		}
		return false;
	}
}
