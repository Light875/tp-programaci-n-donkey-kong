package juego;

import java.awt.Image;
import java.util.Random;
import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Pantalla pantalla;
	private int[] aleatorio = { 100, 180, 260, 300 };
	private Random rd;
	private int cont = 0; // este lo uso para ver la velocidad del tick y POSIBLEMENTE usarlo para los
	private int rand = 0;
	private Image gameover;

	Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "RescateDonkey - Grupo Rojas - Alfonso  - V0.02", 800, 600);
		this.pantalla = new Pantalla(entorno);
		rd = new Random();
		// Inicia el juego!
		this.entorno.iniciar();
		this.gameover = Herramientas.cargarImagen("maxresdefault.jpg");

	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y por lo
	 * tanto es el método más importante de esta clase. Aquí se debe actualizar el
	 * estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */

	public void tick() {
		// Procesamiento de un instante de tiempo

		if (pantalla.enJuego() == true) {

			cont++;
			rand = rd.nextInt(aleatorio.length - 1);
			pantalla.dibujarPantalla(entorno);
			pantalla.moverMario();
			pantalla.curarAlMono();
			pantalla.marioMuerte();
			pantalla.sobrePlataforma(entorno);

			if (cont % aleatorio[rand] == 0) {
				pantalla.generarBarriles(entorno);
				rand = rd.nextInt(aleatorio.length - 1);
			}
		}
		if (pantalla.FinJuego() == true) {
			entorno.dibujarImagen(gameover, 400, 300, 0, 1);
		}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}
